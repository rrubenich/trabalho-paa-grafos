package unioeste.grafos;

import java.awt.Color;
import java.util.ArrayList;

public class Grafo {
	
	private static final int INF = 999999;
	private Vertice[] vertice;
	private Aresta aresta;
	private int max;
	private int size;
	
	private int getVerticeIndex(String nome) {
		for (int i=0; i<size; i++){
			if (nome.equals(vertice[i].getNome())){
				return i;
			}
		}
		return -1;
	}

	public Grafo (int n, Object parent ){
		
		aresta = null;
		this.max = n;
		this.size = 0;
		vertice = new Vertice[max];
		for(int i=0; i<max; i++)
			vertice[i] = null;		
	}
	
	public void add(String nome, int x, int y){
		if (size<max){
			System.out.println("adicionando vertice " + nome);
			vertice[size] = new Vertice(size, nome.toUpperCase(), x, y);
			size++;
		}else
			System.out.println("numero de vertices maior que o informado!");
	}
	
	public boolean addAresta(String source, String target, int w){
		int s = getVerticeIndex(source);
		int t = getVerticeIndex(target);
		if((s==-1) || (t==-1)){
			System.out.println("Vertice de [origem|destino] nao encontrado: "+source+", "+target);
			return false;
		}
		Aresta.append(aresta, s, t, w);
		vertice[s].add( s, t, w);
		return true;
	}
	
	Vertice[] getVertice(){
		return this.vertice;
	}
	
	int getVerticeCount(){
		return this.size;
	}
	
	public void visit(Vertice v){
		Vertice va = null;
		Aresta a;
		
		v.setCor(Color.gray);
		v.setTempoEntrada( (int) System.currentTimeMillis() );
		for(a=v.getAresta(); a!=null; a.getLigacoes()){
			va.setId(a.getDestino());
			if(va.getCor()==Color.white)
				va.setPai(v);
			visit(va);
		}
		v.setTempoSaida( (int) System.currentTimeMillis() );
		v.setCor(Color.black);
	}

	public void bfs(){
	    if(size==0)return;
	    
	    ArrayList<Vertice> L = new ArrayList<Vertice> ();
	    
	    Aresta a;
	    int i;
	    for (i=0; i<size; i++){
	        vertice[i].setPai(null);
	        vertice[i].setCor(Color.white);
	        vertice[i].setDistancia(INF);
	    }
//	    Vertice v = new Vertice(0, null, Color.white);
	    Vertice v = vertice[0];
	    
//	    L.add(v);
	    while(!L.isEmpty()){
		    Vertice verticeA = new Vertice();
	    	v = L.get(0);
	        L.remove(0);
	        
	        for(a=v.getAresta(); a.getLigacoes() != null; a.setOrigem(a.getDestino())){

	            verticeA.setId(a.getDestino());
	            if(verticeA.getCor()==Color.white);
	            verticeA.setCor(Color.gray);
	            verticeA.setPai(v);
	            verticeA.setId(v.getId()+1);
	            L.add(verticeA);            
	        }
	        v.setCor(Color.black);
	    }
	    L.clear();    
	}
	
	public void Prim(int indexVi){
		ArrayList<Vertice> L = new ArrayList<Vertice> ();
		Vertice v = vertice[0], va = null;
		Aresta a;
		
		for(int i = 0; i <v.getN_Arestas(); i++){
			vertice[i].setDistancia(INF);
			vertice[i].setPai(null);;
			vertice[i].setCor(Color.white);
			L.add(vertice[i]);
		}
		vertice[indexVi].setDistancia(0);
		
		while(!L.isEmpty()){
			
			for(a = v.getAresta(); a.getLigacoes() != null; a.setOrigem(a.getDestino())){
				va = vertice[a.getDestino()];
				if(va.getDistancia() > a.getW()){
					if(v.getCor() != Color.black){
						va.setPai(v);
						va.setDistancia(a.getW());
					}
				}
			}
			v.setCor(Color.black);
		}
	L.clear();	
	}	
	
	public void dijkstra(){
	    ArrayList<Vertice> L = new ArrayList<Vertice> ();
	    Vertice v = null;
		Vertice va = null;
		Aresta a;
		int distancia;
	    
		for(int i = 0; i < size; i++){
			vertice[i].setCor(Color.WHITE);
			vertice[i].setPai(null);
			vertice[i].setDistancia(INF);
		}
		
		v.setCor(Color.gray);
		v.setDistancia(0);
	    L.add(v);
	    while(!L.isEmpty()){
	    	va.getAresta();
	    	
	        L.remove(0);
	        for(a = v.getAresta(); a.getLigacoes() != null; a.setOrigem(a.getDestino())){
	            va = vertice[a.getDestino()];
	            distancia = v.getDistancia() + a.getDestino();
	            if(distancia < va.getDistancia()){
	            	va.setDistancia(distancia);
	            	va.setPai(v);
	            }
	            if(va.getCor() == Color.white){
	            	va.setCor(Color.gray);
	            	L.add(va);
	            }
	        }
	    }
	}
	
	public void kruskal(Aresta a){
	    ArrayList<Vertice> vKruskal = new ArrayList<Vertice> ();
	    int nVertices = vertice.length;
		for(int i = 0; i < nVertices; i++){
			Vertice v = new Vertice(i, vertice[i].getNome(), vertice[i].getCoordX(), vertice[i].getCoordY());
			vKruskal.add(v);
		}
		int na = 0;
		while((na-1 < nVertices) && (a != null)){			
			a.setOrigem(a.getDestino());
						
			Vertice vertice1 = vKruskal.get(a.getOrigem());
			Vertice vertice2 = vKruskal.get(a.getDestino());
			
			if(a.getOrigem() >= a.getDestino()){
				vertice1.add(a.getOrigem(), a.getDestino(), a.getW());
				vertice2.add(a.getOrigem(), a.getDestino(), a.getW());
				
				na++;
			}			
		}
	}
	
	public void dfs(Vertice vi){
		Vertice[] v = getVertice();
		int n = getVerticeCount();
		for(int i = 0; i < n; i++){
			v[i].setPai(null);
			v[i].setTempoEntrada(INF);
			v[i].setTempoSaida(INF);
			v[i].setCor(Color.white);
		}
		for(int i = vi.getId(); i < n; i++){
			if(v[i].getCor() == Color.white){
				visit(v[i]);
			}
		}
	}	

	public void ordenacaoTopologica(Vertice vi){
		Vertice[] v = getVertice();
		ArrayList<Vertice> listaOrdenada = new ArrayList<Vertice>();
		int n = getVerticeCount();
		for(int i = 0; i < n; i++){
			v[i].setPai(null);
			v[i].setTempoEntrada(INF);
			v[i].setTempoSaida(INF);
			v[i].setCor(Color.white);
		}
		for(int i = vi.getId(); i < n; i++){
			if(v[i].getCor() == Color.white){
				visit(v[i]);
				listaOrdenada.add(v[i]);
			}
		}
	}	
}