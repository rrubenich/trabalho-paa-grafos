package unioeste.grafos;

import java.util.ArrayList;

public class Aresta {
	private	int origem;		//id1 // vertice de origin
	private int destino;	//id2 // vertice de destino
	private int distancia;  //w
	private ArrayList<Aresta> ligacoes;


	public int getOrigem() {
		return origem;
	}

	public void setOrigem(int origem) {
		this.origem = origem;
	}

	public int getDestino() {
		return destino;
	}

	public void setDestino(int destino) {
		this.destino = destino;
	}

	public int getW() {
		return distancia;
	}

	public void setdistancia(int distancia) {
		this.distancia = distancia;
	}

	public Aresta addLigacao(Aresta o){
		ligacoes.add(o);
		return o;
	}
	
	public void removeLigacacao(Aresta o){
		ligacoes.remove(o);
	}
	
	public ArrayList<Aresta> getLigacoes(){	
		return ligacoes;
	}
	
	public void setLigacoes(ArrayList<Aresta> ligacoes){
		this.ligacoes = ligacoes;
	}	
	
	public Aresta(int origem, int destino, int distancia) {
		super();
		this.origem = origem;
		this.destino = destino;
		this.distancia = distancia;
		ligacoes = new ArrayList<Aresta>();
	}

	public static void append(Aresta aresta, int origem, int destino, int w){
//		if(aresta.getLigacoes().size() != 0){
			aresta = new Aresta ( origem, destino, w ); 
//		}
//		else{
//			append( aresta.addLigacao(aresta), origem, destino, w);
//		}
	}
}