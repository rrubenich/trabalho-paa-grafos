package unioeste.grafos.arquivo;


import java.io.*;

import unioeste.grafos.Grafo;

public class Leitura {
	public Grafo abreArquivo() throws IOException{
		File arquivo = new File( "../files/g1.txt" );

		BufferedReader linhas  = new BufferedReader(new FileReader(arquivo));
		BufferedReader tamanho = new BufferedReader(new FileReader(arquivo));
		
		int tamanhoCount = 0;
		while (tamanho.readLine() != null) tamanhoCount++;
		tamanho.close();
		
		Grafo grafo = new Grafo(tamanhoCount,this);
		
		int vertices = Integer.parseInt(linhas.readLine());
		
		while(linhas.ready()){
			if(vertices > 0){
				String linha = linhas.readLine();
				String item[] = linha.split(",");
				int item1 = Integer.parseInt(item[1]);
				int item2 = Integer.parseInt(item[2]);
				if(linha.length() > 2){
					grafo.add(item[0], item1, item2);
				}
				vertices--;
			}else{
				String linha = linhas.readLine();
				linha = linha.replace("(", "");
				linha = linha.replace(")", "");
				linha = linha.replace(" ", "");
				
				String item[] = linha.split(",");
				int item2 = Integer.parseInt(item[2]);
				grafo.addAresta(item[0], item[1], item2);
			}
		}
		linhas.close();
		
		return grafo;
	}
}