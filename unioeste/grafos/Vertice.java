package unioeste.grafos;

import java.awt.Color;

public class Vertice {
	
	private int id; 
	private String nome;
	private Vertice pai;
	private int tempoEntrada; 
	private int tempoSaida;
	private int distancia;  
	private int n_Arestas; 
	private Color cor; 
	private Aresta aresta;
	private int coordX;
	private int coordY;
	
	public Vertice(int id, String nome, int x, int y) {
		super();
		this.id 	= id;
		this.nome 	= nome;
		this.coordX = x; //cordenadas para desenho
		this.coordY	= y; 
		
		this.tempoEntrada 	= 0;
		this.tempoSaida 	= 0;
		this.distancia 		= 0;
		this.n_Arestas 		= 0;
		this.cor 			= Color.white;
	}
	
	public Vertice(int id, String nome, Vertice pai, int tempoEntrada,
			int tempoSaida, int distancia, int n_Arestas, Color cor,
			Aresta aresta, int coordX, int coordY) {
		this.id = id;
		this.nome = nome;
		this.pai = pai;
		this.tempoEntrada = tempoEntrada;
		this.tempoSaida = tempoSaida;
		this.distancia = distancia;
		this.n_Arestas = n_Arestas;
		this.cor = cor;
		this.aresta = aresta;
		this.coordX = coordX;
		this.coordY = coordY;
	}

	public Vertice(int id, Vertice pai, Color cor) {
		this.cor = cor;
		this.pai = pai;
		this.id = id;
	}
	
	public Vertice(){
		
	}

	public void add(int id1, int id2, int w) {
		Aresta.append(aresta, id1, id2, w);
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Vertice getPai() {
		return pai;
	}
	
	public void setPai(Vertice pai) {
		this.pai = pai;
	}
	
	public int getTempoEntrada() {
		return tempoEntrada;
	}
	
	public void setTempoEntrada(int tempoEntrada) {
		this.tempoEntrada = tempoEntrada;
	}
	
	public int getTempoSaida() {
		return tempoSaida;
	}
	
	public void setTempoSaida(int tempoSaida) {
		this.tempoSaida = tempoSaida;
	}
	
	public int getDistancia() {
		return distancia;
	}
	
	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}
	
	public int getN_Arestas() {
		return n_Arestas;
	}
	
	public void setN_Arestas(int n_Arestas) {
		this.n_Arestas = n_Arestas;
	}
	
	public Color getCor() {
		return cor;
	}
	
	public void setCor(Color cor) {
		this.cor = cor;
	}
	
	public Aresta getAresta() {
		return aresta;
	}
	
	public void setAresta(Aresta aresta) {
		this.aresta = aresta;
	}
	
	public int getCoordX() {
		return coordX;
	}
	
	public void setCoordX(int coordX) {
		this.coordX = coordX;
	}
	
	public int getCoordY() {
		return coordY;
	}
	
	public void setCoordY(int coordY) {
		this.coordY = coordY;
	}

}